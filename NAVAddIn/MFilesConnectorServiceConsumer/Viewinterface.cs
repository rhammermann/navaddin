﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace MFilesConnectorServiceConsumer
{
    public class ViewInterface {

        private string baseUrl = "http://monitor-ws.baymain.com/api/";

        private string authUrl = "account/login";
        private string getTenantUrl = "ERPVault/GetTenantName";
        private string viewPoUrl = "ERPVault/ViewAPPurchaseOrder";

        private string _authToken = null;

        public string GetAuthToken(string userName, string passWord, string tenant) {
            var client = new HttpClient();
            client.BaseAddress = new Uri($"{baseUrl}{authUrl}");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync($"?username={userName}&password={passWord}&tenantalias={tenant}").Result; // Blocking call!
            if (response.IsSuccessStatusCode) {
                // Parse the response body. Blocking!
                var authResponse  = response.Content.ReadAsStringAsync().Result;

                _authToken = JToken.Parse(authResponse)["token"].ToString();
            }
            else {
                Console.WriteLine("{0} ({1})", (int) response.StatusCode, response.ReasonPhrase);
            }
            return _authToken;
        }

        //------------------------------------------------------------------------------------------------------------------------
        public string GetTenantName(string userName, string passWord, string tenant) {
            _authToken = _authToken == null ? GetAuthToken(userName, passWord, tenant) : _authToken;

            var client = new HttpClient();
            client.BaseAddress = new Uri($"{baseUrl}");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Add("Authorization", _authToken);


            // List data response.
            HttpResponseMessage response = client.GetAsync($"{getTenantUrl}").Result;
            if (!response.IsSuccessStatusCode)
            {
                var authResponse = response.Content.ReadAsStringAsync().Result;
                return authResponse;
            }
            else {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }
            return string.Empty;
        }



        public void ShowPurchaseOrder(string poName, string userName = "", string passWord = "", string tenant = "") {
            _authToken = _authToken == null ? GetAuthToken(userName, passWord, tenant) : _authToken;

            var client = new HttpClient();
            client.BaseAddress = new Uri($"{baseUrl}{viewPoUrl}");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Add("Authorization", _authToken);


            // List data response.
            HttpResponseMessage response = client.PostAsync(viewPoUrl, null).Result;
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }
            return;


        }
    }
}
