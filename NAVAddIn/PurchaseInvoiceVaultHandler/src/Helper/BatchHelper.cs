﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFiles.VAF.Common;
using MFilesAPI;
using Newtonsoft.Json.Linq;
using Toolbox;

namespace PurchaseInvoiceVaultHandler.src.Helper
{
    public class BatchHelper {
        private Vault _vault;
        private InvoiceHelper _invoiceHelper;


        public BatchHelper(Vault vault) {
            _vault = vault;
            _invoiceHelper = new InvoiceHelper(_vault);

        }
        public ObjectSearchResults LocateActiveBatches()
        {
            var historyBatchId =
                SearchHelpers.GetValuelistItemOfProperty(_vault, AliasHelpers.GetPropertyId(_vault, "PD_BatchStatusSelect"),
                    "History");

            var batchSearchParms = new List<SearchParm>() {
                new SearchParm() {
                    PropertyDef = AliasHelpers.GetPropertyId(_vault, "PD_IsProcessed"),
                    MfDataType = MFDataType.MFDatatypeBoolean,
                    MfConditionType = MFConditionType.MFConditionTypeEqual,
                    PropertyValue = false
                },
                new SearchParm() {
                    PropertyDef = AliasHelpers.GetPropertyId(_vault, "PD_BatchStatusSelect"),
                    MfConditionType = MFConditionType.MFConditionTypeNotEqual,
                    MfDataType = MFDataType.MFDatatypeLookup,
                    PropertyValue = new Lookup() {
                        Item = historyBatchId,
                        ObjectType = AliasHelpers.GetObjectTypeId(_vault, "VLBatchstatus")
                    }
                }
            };

            var foundBatches = SearchHelpers.SearchForPropertyValue(_vault, batchSearchParms.ToArray(),
                AliasHelpers.GetClassId(_vault, "CLBatch"));
            return foundBatches;
        }



        public string ProcessBatch(EventHandlerEnvironment env, ObjVer batchToProceed) {
            _vault = env.Vault;
            ////-----------------------------------------------------
            //// Get source parms
            //var Jsondata = JObject.Parse(env.Input);

            //var jObjVer = Jsondata.SelectToken("ObjVer");

            //var batchToProceed = new ObjVer() {
            //    ID = jObjVer.SelectToken("ID").ToObject<int>(),
            //    Type = jObjVer.SelectToken("Type").ToObject<int>(),
            //    Version = jObjVer.SelectToken("Version").ToObject<int>()
            //};

            //-------------------------------------------------------------------------
            // Start searching all invoices belonging to a batch
            // -- check status, only approved is valid and not transfered
            //------------------------------------------------------------------------

            var invoiceSearchParms = new List<SearchParm>() {
                new SearchParm() {
                    PropertyDef = AliasHelpers.GetPropertyId(env.Vault, "PDBatchSelect"),
                    MfDataType = MFDataType.MFDatatypeLookup,
                    MfConditionType = MFConditionType.MFConditionTypeEqual,
                    PropertyValue = new Lookup() {
                        Item = batchToProceed.ID,
                        Version = batchToProceed.Version,
                        ObjectType = batchToProceed.Type
                    }
                },
                new SearchParm() {
                    // Voucher/Receipt # (ERP)
                    PropertyDef = AliasHelpers.GetPropertyId(_vault, "VoucherNumberERP"),
                    MfDataType = MFDataType.MFDatatypeText,
                    MfConditionType = MFConditionType.MFConditionTypeEqual,
                    PropertyValue = String.Empty
                }, 
                //new SearchParm() {
                //    // InvoiceTransfered (ERP)
                //    PropertyDef = AliasHelpers.GetPropertyId(_vault, "")
                //}
            };

            var foundInvoiceHeaders = SearchHelpers.SearchForPropertyValue(env.Vault, invoiceSearchParms.ToArray(),
                AliasHelpers.GetClassId(env.Vault, "CLAPInvoiceData"));

            if (foundInvoiceHeaders == null || foundInvoiceHeaders.Count == 0) {
                return String.Empty;
            }
            // for each found invoice add header and lines 
            try {
                var invoiceResult = _invoiceHelper.ProcessInvoice(foundInvoiceHeaders);
            }
            catch (Exception e) {
                Console.WriteLine(e);
                throw new ApplicationException("ProcessInvoice failed", e);
            }

            return "OK";
        }

        public void SetBatchStatus(ObjVer batch, bool batchSuccessStatus) {
            ObjVer checkedOutBatch;
            bool wasCheckedOut = false;
            if (_vault.ObjectOperations.IsCheckedOut(batch.ObjID)) {
                checkedOutBatch = batch;
                wasCheckedOut = true;
            }
            else {
                checkedOutBatch = _vault.ObjectOperations.CheckOut(batch.ObjID).ObjVer;
            }

            var historyId = SearchHelpers.GetValuelistItemOfProperty(_vault,
                AliasHelpers.GetPropertyId(_vault, "PD_BatchStatusSelect"), "History");
            var errorId = SearchHelpers.GetValuelistItemOfProperty(_vault,
                AliasHelpers.GetPropertyId(_vault, "PD_BatchStatusSelect"), "Error");

            var batchStatusId = batchSuccessStatus? historyId : errorId;


            var batchStatusPropertyValue = new PropertyValue();
            batchStatusPropertyValue.PropertyDef = AliasHelpers.GetPropertyId(_vault, "PD_BatchStatusSelect");
            batchStatusPropertyValue.TypedValue.SetValueToLookup(new Lookup() {
                Item = batchStatusId,
                ObjectType = AliasHelpers.GetObjectTypeId(_vault, "VLBatchstatus")
            });

            var batchProcessedPropertyValue = new PropertyValue();
            batchProcessedPropertyValue.PropertyDef = AliasHelpers.GetPropertyId(_vault, "PD_IsProcessed");
            batchProcessedPropertyValue.TypedValue.SetValue(MFDataType.MFDatatypeBoolean, true);


            try {
                _vault.ObjectPropertyOperations.SetProperty(checkedOutBatch, batchStatusPropertyValue);

                if (batchSuccessStatus) {
                    _vault.ObjectPropertyOperations.SetProperty(checkedOutBatch, batchProcessedPropertyValue);
                }

                if (!wasCheckedOut)
                {
                    _vault.ObjectOperations.CheckIn(checkedOutBatch);
                }
            }
            catch (Exception e) {
                SysUtils.ReportErrorToEventLog($"Error updating status of batch {checkedOutBatch.ID}", e);
            }
        }
    }
}
