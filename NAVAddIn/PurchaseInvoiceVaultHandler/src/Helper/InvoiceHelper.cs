﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFilesAPI;
using NAVMfilesConnection.BayPurchaseInvoicePageService;
using PurchaseInvoiceVaultHandler.Helper;
using Toolbox;
using Type = System.Type;

namespace PurchaseInvoiceVaultHandler.src.Helper
{
    public class InvoiceHelper {
        private Vault _vault;
        private ObjectHelper _objectHelper;
        private NAV.BAYPurchaseInvoice _invoiceToCreate;


        public InvoiceHelper(Vault vault) {
            _vault = vault;
            _objectHelper = new ObjectHelper(_vault);

        }

        public string ProcessInvoice(ObjectSearchResults foundInvoiceHeaders) {
            foreach (ObjectVersion foundInvoiceHeader in foundInvoiceHeaders) {
                // Extract property data from AP Invoice Data
                try {
                    _invoiceToCreate = FillInvoiceFromMetadata(foundInvoiceHeader.ObjVer);
                }
                catch (Exception e) {
                    Console.WriteLine(e);
                    return "Error";
                }

                // put invoice data into NAV and and get created invoice back
                var navInvoiceService = new NAVMfilesConnection.ERPDataProviderNAV();
                try {
                    _invoiceToCreate = navInvoiceService.AddPurchaseInvoiceHeader(ref _invoiceToCreate);
                }
                catch (Exception e) {
                    Console.WriteLine(e);
                    throw new ApplicationException("CreatePurchaseInvoiceHeader failed", e);
                }
                //-----------------------------------------------------
                // collect all invoice lines from MDM
                var lines = CollectLinesForInvoice(foundInvoiceHeader.ObjVer);
                //-----------------------------------------------------
                // initialize Lines member object
                _invoiceToCreate.PurchLines = new Purch_Invoice_Line[lines.Count];

                for (int idx = 0; idx < _invoiceToCreate.PurchLines.Length; idx++)
                    _invoiceToCreate.PurchLines[idx] = new Purch_Invoice_Line();

                //--------------------------------------------------------------------
                // populate all lines and
                int i = 0;
                foreach (var line in _invoiceToCreate.PurchLines) {
                    _invoiceToCreate.PurchLines[i] = lines[i++];
                }

                //--------------------------------------------------------------------
                // update the header with the lines
                try {
                    var updatedHeader = navInvoiceService.UpdatePurchaseInvoiceHeader(_invoiceToCreate);
                }
                catch (Exception e) {
                    Console.WriteLine(e);
                    throw new ApplicationException("UpdateHeader failed", e);
                }

                //--------------------------------------------------------------------
                // TODO: TBD
                // update M-Files with Voucher/Receipt # (ERP), InvoiceTransfered (ERP), GenerationErrorMessage


                return "OK";
            }
            return "Error";
        }



        public List<Purch_Invoice_Line> CollectLinesForInvoice(ObjVer foundInvoice)
        {
            var lines = new List<Purch_Invoice_Line>();

            var relatedObjects =
                _vault.ObjectOperations.GetRelationships(foundInvoice, MFRelationshipsMode.MFRelationshipsModeAll);

            var relatedLines = relatedObjects.Cast<ObjectVersion>()
                .Where(x => x.Class == AliasHelpers.GetClassId(_vault, "CLAPInvoiceLine"));

            foreach (ObjectVersion relatedLine in relatedLines)
            {
                // load data from invoice line
                var filledInvoiceLine = FillInvoiceLineFromMetadata(relatedLine.ObjVer);
                lines.Add(filledInvoiceLine);
            }

            return lines;
        }



        public NAV.BAYPurchaseInvoice FillInvoiceFromMetadata(ObjVer foundInvoiceHeader)
        {
            var invoice = new NAV.BAYPurchaseInvoice();

            String vendorName = GetVendorNameFromHeader(foundInvoiceHeader);
            invoice.Buy_from_Vendor_Name = vendorName;

            invoice.Currency_Code = _objectHelper.GetValueFromProperty(foundInvoiceHeader, "PD_CurrencySelect", typeof(String));
            invoice.Document_Date = _objectHelper.GetValueFromProperty(foundInvoiceHeader, "PDDocumentDate", typeof(DateTime));
            invoice.Vendor_Invoice_No = _objectHelper.GetValueFromProperty(foundInvoiceHeader, "VendorDocNum", typeof(String));




            // TODO: Make it complete

            return invoice;
        }

        private string GetVendorNameFromHeader(ObjVer foundInvoiceHeader) {

            var vendorProp = _vault.ObjectPropertyOperations.GetProperty(foundInvoiceHeader, AliasHelpers.GetPropertyId(_vault, "VendorSelect"));
            var vendorObjID = new ObjID();
            vendorObjID.ID = vendorProp.TypedValue.GetLookupID();
            vendorObjID.Type = vendorProp.TypedValue.GetValueAsLookup().ObjectType;
            var currentVendor = _vault.ObjectOperations.GetLatestObjVer(vendorObjID, true, false);

            var vendorName =
                _vault.ObjectPropertyOperations.GetProperty(currentVendor, AliasHelpers.GetPropertyId(_vault, "AName1"));
            if (vendorName != null) {
                return vendorName.GetValueAsUnlocalizedText();
            }
            else {
                return String.Empty;
            }

        }

        public Purch_Invoice_Line FillInvoiceLineFromMetadata(ObjVer foundInvoiceLine)
        {
            var invoiceLine = new Purch_Invoice_Line()
            {
                Type = ServiceLayer.Model.InvoiceHeader.Type.Item,
                TypeSpecified = true,

                Quantity = _objectHelper.GetValueFromProperty(foundInvoiceLine, "PDQuantity", typeof(decimal)),
                QuantitySpecified = true,

                Direct_Unit_Cost = _objectHelper.GetValueFromProperty(foundInvoiceLine, "PDUnitPrice", typeof(int)),
                Direct_Unit_CostSpecified = true,

                Description = _objectHelper.GetValueFromProperty(foundInvoiceLine, "PDItemSelect", typeof(string)),

                Unit_Price_LCY = _objectHelper.GetValueFromProperty(foundInvoiceLine, "PDUnitPrice", typeof(decimal)),
                Unit_Price_LCYSpecified = true,

                Qty_Assigned = _objectHelper.GetValueFromProperty(foundInvoiceLine, "PDQuantity", typeof(int)),
                Qty_AssignedSpecified = true

            };




            // TODO: Make it complete

            return invoiceLine;
        }

    }
}
