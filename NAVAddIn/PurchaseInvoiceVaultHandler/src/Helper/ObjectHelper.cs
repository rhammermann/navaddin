﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFilesAPI;
using Toolbox;

namespace PurchaseInvoiceVaultHandler.Helper {
    public class ObjectHelper {
        private Vault _vault;

        public ObjectHelper(Vault vault) {
            _vault = vault;
        }
        public static ObjVer LookupVendor(Vault vault, string vendorName) {
            
            List<SearchParm> searchParms = new List<SearchParm>() {
                new SearchParm() {
                    PropertyDef = AliasHelpers.GetPropertyId(vault, "AName1"),
                    MfConditionType = MFConditionType.MFConditionTypeEqual,
                    MfDataType = MFDataType.MFDatatypeText,
                    PropertyValue = vendorName
                }
            };

            var searchResults = SearchHelpers.SearchForPropertyValue(vault, searchParms.ToArray(), AliasHelpers.GetClassId(vault, "AVendor"));

            if (searchResults != null && searchResults.Count != 0) {

                var foundObject = searchResults.Cast<ObjectVersion>().FirstOrDefault();
                return foundObject.ObjVer;
            }

            return null;
        }

        /// <summary>
        /// Helper for getting values from M-Files properties
        /// </summary>
        /// <param name="objVer"></param>
        /// <param name="aliasName"></param>
        /// <param name="myType"></param>
        /// <returns></returns>



        public dynamic GetValueFromProperty(ObjVer objVer, string aliasName, System.Type myType)
        {

            var prop = _vault.ObjectPropertyOperations.GetProperty(objVer,
                AliasHelpers.GetPropertyId(_vault, aliasName));

            object currentValue = null;
            if (prop.TypedValue.Value is System.DBNull)
            {
                currentValue = null;
            }
            else
            {
                if (myType == typeof(String))
                {
                    if (prop.TypedValue.DataType == MFDataType.MFDatatypeLookup)
                    {
                        return prop.GetValueAsUnlocalizedText();
                    }
                    else
                    {
                        return prop.TypedValue.Value.ToString();
                    }


                }
                if (myType == typeof(int))
                {
                    return int.Parse(prop.TypedValue.Value.ToString());
                }
                if (myType == typeof(decimal))
                {
                    return decimal.Parse(prop.TypedValue.Value.ToString());
                }
                if (myType == typeof(DateTime))
                {
                    return (DateTime)prop.TypedValue.Value;
                }

            }
            return currentValue;
        }

    }
}
