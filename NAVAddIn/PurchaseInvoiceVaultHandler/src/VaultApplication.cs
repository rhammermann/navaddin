﻿using System;
using System.Collections.Generic;
using System.Linq;
using MFiles.VAF;
using MFiles.VAF.Common;
using MFilesAPI;
using Newtonsoft.Json.Linq;
using ServiceLayer;
using ServiceLayer.Model.InvoiceHeader;
using ServiceLayer.Model.InvoiceLines;
using Toolbox;
using Type = ServiceLayer.Model.InvoiceHeader.Type;
using AutoMapper;
using NAVMfilesConnection.BayPurchaseInvoicePageService;
using PurchaseInvoiceVaultHandler.Helper;
using PurchaseInvoiceVaultHandler.src.Helper;
using ServiceLayer.Model.MdmObjects;

namespace PurchaseInvoiceVaultHandler
{

    /// <summary>
    /// Simple vault application to demonstrate VAF.
    /// </summary>
    public class PurchaseInvoiceHandlerApplication : VaultApplicationBase {
        private Vault _vault;

        protected override void StartApplication()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<PropertyValue, string>().ConvertUsing(new PropertyValueTypeConverter());
                cfg.CreateMap<ApInvoiceData, BAYPurchaseInvoice>()
                    .ForMember(dest => dest.Buy_from_Vendor_Name, opt => opt.ResolveUsing(x => x.BuyFromVendorName));
            });
            // Fetch Parameter
        }


        // TODO: TBD
        [VaultExtensionMethod("ScanForActiveBatchesExtensionMethod", RequiredVaultAccess = MFVaultAccess.MFVaultAccessNone)]
        private string ScanForActiveBatchesExtensionMethod(EventHandlerEnvironment env) {

            var batchHelper = new BatchHelper(env.Vault);
            var foundBatches = batchHelper.LocateActiveBatches();
            if (foundBatches == null || foundBatches.Count == 0) {
                return String.Empty;
            }

            // for each found invoice add header and lines 
            foreach (ObjectVersion foundBatch in foundBatches) {
                // Proceed all invoices in batch and record error if one
                try {

                    var batchResult = batchHelper.ProcessBatch(env, foundBatch.ObjVer);
                    if (batchResult != "OK") {
                        batchHelper.SetBatchStatus(foundBatch.ObjVer, false);
                        continue;
                    }
                    
                    //-------------------------------------------------------
                    // Mark batch as completed (History)
                    batchHelper.SetBatchStatus(foundBatch.ObjVer, true);
                }
                catch (Exception e) {
                    //-------------------------------------------------------
                    // Mark batch as failed(Error)
                    batchHelper.SetBatchStatus(foundBatch.ObjVer, false);

                    SysUtils.ReportErrorToEventLog(e);
                    // Log error
                }
            }
            return String.Empty;
        }









    }
}