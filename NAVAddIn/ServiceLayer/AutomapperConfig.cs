﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MFilesAPI;
using ServiceLayer.Model.InvoiceHeader;
using ServiceLayer.Model.MdmObjects;

//using ServiceDataLayer;

namespace ServiceLayer
{
    public class MapMdmToNavPurchaseInvoiceHeaderProfile : Profile
    {
        public MapMdmToNavPurchaseInvoiceHeaderProfile()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ApInvoiceData, BAYPurchaseInvoice>();
            });
        }
    }
    public class MapMdmToNavPurchaseInvoiceLineProfile : Profile
    {
        public MapMdmToNavPurchaseInvoiceLineProfile()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ApInvoiceLine, Purch_Invoice_Line>();
            });
        }
    }

    public class NavMdmToMdmProfile : Profile
    {
        public NavMdmToMdmProfile()
        {
            //Mapper.Initialize(cfg => {
            //    cfg.CreateMap<PurchaseInvoiceService.BAYPurchaseInvoice, InvoiceHeader>();
            //});
        }

    }

    public class PropertyValueTypeConverter : ITypeConverter<PropertyValue, string>
    {
        public string Convert(PropertyValue source, string destination, ResolutionContext context)
        {

            string currentValue;
            if (source.TypedValue.Value is System.DBNull)
            {
                currentValue = null;
            }
            else
            {
                if (source.TypedValue.DataType == MFDataType.MFDatatypeLookup)
                {
                    currentValue = source.GetValueAsUnlocalizedText();
                }
                else
                {
                    currentValue = source.GetValueAsUnlocalizedText();
                }
            }
            return currentValue;
        }
    }

    public class CustomValueResolver : IValueResolver<PropertyValue, string, int>
    {
        public int Resolve(PropertyValue source, string destination, int member, ResolutionContext context)
        {
            return 0;
        }
    }
}
