﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Helpers
{
    public class BehaviorHelper : Attribute, IEndpointBehavior 
    {
        public void Validate(ServiceEndpoint endpoint) {
            Console.WriteLine("Validate");
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) {
            Console.WriteLine("AddBinding");
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) {
            Console.WriteLine("ApplyDisptach");
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime) {

            EndpointBehaviorElementCollection coll = new EndpointBehaviorElementCollection();
            coll.Add(new EndpointBehaviorElement() {
                new ClientCredentialsElement() {
                    
                    Windows = {
                        AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation,
                        AllowNtlm = true
                    }
                }
            });

        }
    }
}
