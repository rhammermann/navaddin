﻿namespace ServiceLayer.Model
{
    public class DestinationFieldObject
    {
        public DestinationFieldObject(DestinationType type, string name, string value) {
            Destination = type;
            Name = name;
            Value = value;
        }
        public enum DestinationType {
            MFiles,
            NAVISION,
            BOTH
        }

        public DestinationType Destination { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
