﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFilesAPI;

namespace ServiceLayer.Model.MdmObjects
{
    public class ApInvoiceData
    {
        public PropertyValue Amount { get; set; }
        public PropertyValue CurrencyCode { get; set; }
        public PropertyValue DocumentDate { get; set; }
        public PropertyValue VendorInvoiceNo { get; set; }
        public PropertyValue BuyFromVendorName { get; set; }
    }
}
