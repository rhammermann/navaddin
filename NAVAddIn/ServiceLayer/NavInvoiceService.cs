﻿#define LOCAL

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Baystream.MDM.Common;
using Microsoft.OData.Client;
using ServiceLayer.Helpers;
using ServiceLayer.Model;
using ServiceLayer.Model.InvoiceHeader;
using ServiceLayer.Model.InvoiceLines;
using ServiceLayer.Model.MdmObjects;
using Create_Result = ServiceLayer.Model.InvoiceHeader.Create_Result;
using Update_Result = ServiceLayer.Model.InvoiceHeader.Update_Result;

namespace ServiceLayer
{
    public class InvoiceCreationService {
        private VaultElement _ve;


        //public InvoiceCreationService() {
        //    MDMConfiguration mdmConfig = new MDMConfiguration();
        //    mdmConfig.ReadConfiguration();
        //    string vaultguid = "0F0E68E0-54EF-4DA5-9472-D5B9FDBD3BD1";
        //    _ve = mdmConfig.GetDefaultVaultElementForVault(vaultguid);

        //}


        //public string AddPurchaseInvoiceHeaderAndLine()
        //{

        //    var invoice = new BAYPurchaseInvoice();
        //    var invoiceLine = new BAYPurchaseInvoiceLines();

        //    GetMetadataHeaderFromFile(
        //        ref invoice,
        //        @"C:\Users\rh\OneDrive\Baystream\MDM\ScannerInput\Data_00000023.xml",
        //        "_Invoice");
        //    var nLines = GetMetadataLinesFromFile(
        //        @"C:\Users\rh\OneDrive\Baystream\MDM\ScannerInput\Data_00000023.xml",
        //        "_LineItems");


        //    var key = CreatePurchaseInvoiceHeader(ref invoice);
        //    //------------------------------------------------------------------

        //    invoice.PurchLines = new Purch_Invoice_Line[nLines.Count];

        //    for (int idx = 0; idx < invoice.PurchLines.Length; idx++)
        //        invoice.PurchLines[idx] = new Purch_Invoice_Line();

        //    UpdatePurchaseInvoiceHeader(ref invoice);
        //    //--------------------------------------------------------------------
        //    int i = 0;
        //    foreach (var line in invoice.PurchLines)
        //    {
        //        invoice.PurchLines[i] = nLines[i++];
        //    }
        //    var updateKey = UpdatePurchaseInvoiceHeader(ref invoice);
        //    return updateKey;
        //}

        //public string CreatePurchaseInvoiceHeader(NAV.BAYPurchaseInvoice invoice) {
        //    try
        //    {
        //        var conn = new NAVMfilesConnection.ERPDataProviderNAV(_ve.name, _ve.erpuser, _ve.erpuserdomain, _ve.erppassword, _ve.erpserver, _ve.navinstance);
        //        var headerNo = conn.AddPurchaseInvoiceHeader(ref invoice);
        //        return headerNo;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw new ApplicationException("CreateHeader failed!", e);
        //    }
        //}
        //public DataServiceResponse UpdatePurchaseInvoiceHeader(APInvoice invoice) {
        //    Update_Result updateResult;
        //    try {
        //        var pi = AutoMapper.Mapper.Map<APInvoice, NAV.BAYPurchaseInvoice>(invoice);
        //        var conn = new NAVMfilesConnection.ERPDataProviderNAV(_ve.name, _ve.erpuser, _ve.erpuserdomain, _ve.erppassword, _ve.erpserver, _ve.navinstance);
        //        var response = conn.UpdatePurchaseInvoice(pi);
        //        return response;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw new ApplicationException("UpdateHeader failed!", e);
        //    }
        //}


        //public DataServiceResponse UpdatePurchaseInvoiceLine(ApInvoiceLine invoiceLine)
        //{
        //    try
        //    {
        //        var line = AutoMapper.Mapper.Map<ApInvoiceLine, NAV.BAYPurchaseInvoicePurchLines>(invoiceLine);

        //        var conn = new NAVMfilesConnection.ERPDataProviderNAV(_ve.name, _ve.erpuser, _ve.erpuserdomain, _ve.erppassword, _ve.erpserver, _ve.navinstance);
        //        var result = conn.UpdatePurchaseInvoiceLine(line);


        //        return result;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        return null;
        //    }
        //}


    }
}
