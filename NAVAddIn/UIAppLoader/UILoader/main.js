
var currentlySelectedItems = null;
var currentVault = null;
var _shellFrame = null;
var _result = null;

function OnNewShellUI(shellUI) {
    /// <summary>The entry point of ShellUI module.</summary>
    /// <param name="shellUI" type="MFiles.ShellUI">The new shell UI object.</param>

    // Register to listen new shell frame creation event.
    shellUI.Events.Register(Event_NewShellFrame, newShellFrameHandler);
}

function newShellFrameHandler(shellFrame) {
    /// <summary>Handles the OnNewShellFrame event.</summary>
    /// <param name="shellFrame" type="MFiles.ShellFrame">The new shell frame object.</param>

    // Register to listen to the started event.
    shellFrame.Events.Register(Event_Started, getShellFrameStartedHandler(shellFrame));
    shellFrame.Events.Register(Event_NewShellListing, getNewShellListingHandler(shellFrame));
}

function getShellFrameStartedHandler(shellFrame) {
    /// <summary>Gets a function to handle the Started event for shell frame.</summary>
    /// <param name="shellFrame" type="MFiles.ShellFrame">The current shell frame object.</param>
    /// <returns type="MFiles.Events.OnStarted">The event handler.</returns>

    // Return the handler function for ShellFrame's Started event.
    return function () {
        currentVault = shellFrame.ShellUI.Vault;
        _shellFrame = shellFrame;

        // Let's create a button.
        var moveItemsCommandId = shellFrame.Commands.CreateCustomCommand("Load Invoice Batch");

        // Add it to the task area.
        // ref: http://www.m-files.com/UI_Extensibility_Framework/index.html#MFClientScript~ITaskPane~AddCustomCommandToGroup.html
        shellFrame.TaskPane.AddCustomCommandToGroup(moveItemsCommandId, TaskPaneGroup_Main, 0);

        // Register to respond to events.
        shellFrame.Commands.Events.Register(Event_CustomCommand,
            function(command) {
                // We only care about our command.
                if (command !== moveItemsCommandId)
                    return;

                //// Ensure we have items to process.
                //if (null === currentlySelectedItems)
                //    return;


                ////------------------------------------------------------------------------
                //var wfParm = {
                //    "ObjVer": {}
                //};

                //wfParm.ObjVer = currentlySelectedItems.ObjectVersions.Item(1).ObjVer;
                //var wfParmString = JSON.stringify(wfParm);

                var parmString = 'Empty';
                _result = currentVault.ExtensionMethodOperations.ExecuteVaultExtensionMethod('ScanForActiveBatchesExtensionMethod', parmString);

            });


    };
}


function getNewShellListingHandler(shellFrame) {
    /// <summary>Gets a function to handle the NewShellListing event for shell frame.</summary>
    /// <param name="shellFrame" type="MFiles.ShellFrame">The current shell frame object.</param>
    /// <returns type="MFiles.Events.OnNewShellListing">The event handler.</returns>

    // Return the handler function for NewShellListing event.
    return function (shellListing) {

        // Listen for selection change events on the listing
        shellListing.Events.Register(Event_SelectionChanged, function (selectedItems) {

            // Sanity.
            currentlySelectedItems = null;
            if(selectedItems.ObjectVersions.Count === 0)
                return;

            // Log the selected items.
            currentlySelectedItems = selectedItems;
        });
    };
}





