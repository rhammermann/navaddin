﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFiles.VAF;
using MFiles.VAF.Common;
using MFilesAPI;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UniProject
{
    /// <summary>
    /// Simple vault application to demonstrate VAF.
    /// </summary>
    public class VaultApplication : VaultApplicationBase
    {
        /// <summary>
        /// Install nested UX App via API if not already installed
        /// </summary>
        /// <param name="vault"></param>
        protected override void InitializeApplication(Vault vault)
        {
            try
            {
                string appPath = "UILoader.zip";
                if (File.Exists(appPath))
                {
                    vault.CustomApplicationManagementOperations.InstallCustomApplication(appPath);
                }
                else
                {
                    SysUtils.ReportErrorToEventLog("File: " + appPath + " does not exist");
                }
            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog(ex.Message);
            }

            base.InitializeApplication(vault);
        }


    }

}